//Modal
var modal = document.getElementById("modal"),
	modalTriggerOne = document.getElementById("modal-trigger-one"),
	modalTriggerTwo = document.getElementById("modal-trigger-two"),
	modalClose = document.getElementsByClassName("modal-close-btn");

modalTriggerOne.onclick = function() {
	modal.classList.add("modal-show");
	modal.classList.remove("modal-flipped");
};

modalTriggerTwo.onclick = function() {
	modal.classList.add("modal-flipped");
};

for (var i = 0; i < modalClose.length; i++) {
	modalClose[i].onclick = function (){
		modal.classList.remove("modal-show");
	}
};

document.onkeydown = function(evt) {
	evt = evt || window.event;
	if (evt.keyCode == 27) {
		modal.classList.remove("modal-show");
	}
};

window.onclick = function(event) {
	if (event.target == modal) {
		modal.classList.remove("modal-show");
	}
};

//Add animation on mouse wheel
var slideOne = document.getElementById("slide-one"),
	slideTwo = document.getElementById("slide-two"),
	slideThree = document.getElementById("slide-three"),
	headingList= document.getElementById("slide-heading-list"),
	slideShow = document.getElementById("slide-show");
var nextslideindex = 0;
var arr = [];
arr.length = 6;

function onChangeSlide(){
	nextslideindex = (nextslideindex < 0 ) ? 0 : (nextslideindex > arr.length - 1) ? arr.length - 1 : nextslideindex;

	if (nextslideindex == 0) {
		headingList.children[0].classList.add("active");
		headingList.children[1].classList.remove("active");
	} else if (nextslideindex == 1) {
		headingList.children[0].classList.remove("active");
		headingList.children[1].classList.add("active");
		headingList.children[2].classList.remove("active");
	} else if (nextslideindex == 2) {
		headingList.children[1].classList.remove("active");
		headingList.children[2].classList.add("active");
		slideOne.classList.remove("slide-fade");
		slideTwo.classList.remove("slide-open");
	} else if (nextslideindex == 3) {
		slideOne.classList.add("slide-fade");
		slideTwo.classList.add("slide-open");
		slideThree.classList.remove("slide-open");
	} else if (nextslideindex == 4) {
		slideThree.classList.add("slide-open");
	}
}

function onMouseWheel(e){
	var evt = window.event || e;
	var delta = evt.detail ? evt.detail*(-120) : evt.wheelDelta;
	nextslideindex = (delta <= -120) ? nextslideindex + 1 : nextslideindex - 1;

	onChangeSlide(nextslideindex);

	e.preventDefault ? e.preventDefault() : (e.returnValue = false);
}

var mousewheelevt = (/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel";

var _onMouseWheel = debounce(onMouseWheel, 400);
if (slideShow.attachEvent) slideShow.attachEvent("on" + mousewheelevt, _onMouseWheel); else slideShow.addEventListener(mousewheelevt, _onMouseWheel, false);

//Detect a finger swipe
document.addEventListener('touchstart', handleTouchStart, false);
document.addEventListener('touchmove', handleTouchMove, false);

var xDown = null;
var yDown = null;

function handleTouchStart(evt) {
	xDown = evt.touches[0].clientX;
	yDown = evt.touches[0].clientY;
};

function handleTouchMove(evt) {
	if ( ! xDown || ! yDown ) {
		return;
	}

	var yUp = evt.touches[0].clientY;

	var yDiff = yDown - yUp;

	if ( yDiff > 0 ) {
		nextslideindex += 1;
	} else {
		nextslideindex -= 1;
	}

	xDown = null;
	yDown = null;
};

//Defer function call
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};